from setuptools import setup

setup(name='anytimebot',
      version='0.1',
      description='Anytime bot',
      url='https://gitlab.com/mvettosi/anytime-tournament-bot',
      author='Matteo Vettosi',
      author_email='matteo.vettosi@gmail.com',
      license='MIT',
      packages=['anytimebot'],
      zip_safe=False)
